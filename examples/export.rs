//! Demonstrates how to export a certring.

use std::io::Write;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let certd = pgp_cert_d::CertD::new()?;

    let args = std::env::args().collect::<Vec<_>>();
    if args.len() != 1 {
        panic!("Usage: {0} >CERTRING  -or-  {0} | gpg --import", args[0]);
    }

    let mut sink = std::io::stdout();
    for fp in certd.iter_fingerprints()? {
        if let Some((_, data)) = certd.get(fp)? {
            sink.write_all(&data)?;
        }
    }

    Ok(())
}
