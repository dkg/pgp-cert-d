//! Demonstrates how to import a certring.

use sequoia_openpgp as openpgp;
use openpgp::{
    *,
    cert::prelude::*,
    parse::Parse,
    serialize::SerializeInto,
};

fn main() -> Result<()> {
    let certd = pgp_cert_d::CertD::new()?;

    let args = std::env::args().collect::<Vec<_>>();
    if args.len() != 1 {
        panic!("Usage: {0} <CERTRING  -or-  gpg --export | {0}", args[0]);
    }

    for cert in CertParser::from_reader(std::io::stdin())? {
        let mut cert = cert?;

        if let Some((_modified, c)) = certd.get(cert.fingerprint())? {
            let c = Cert::from_bytes(&c)?;
            cert = cert.merge_public(c)?;
        }

        certd.insert(cert.fingerprint(), cert.to_vec()?)?;
    }

    Ok(())
}
