//! Demonstrates how to insert a certificate.

use sequoia_openpgp as openpgp;
use openpgp::{
    *,
    parse::Parse,
    serialize::SerializeInto,
};

fn main() -> Result<()> {
    let certd = pgp_cert_d::CertD::new()?;

    let args = std::env::args().collect::<Vec<_>>();
    if args.len() != 2 {
        panic!("Usage: {} <CERTFILE>", args[0]);
    }

    let mut cert = Cert::from_file(&args[1])?;

    if let Some((_modified, c)) = certd.get(cert.fingerprint())? {
        let c = Cert::from_bytes(&c)?;
        cert = cert.merge_public(c)?;
    }

    certd.insert(cert.fingerprint(), cert.to_vec()?)?;
    Ok(())
}
