use std::{
    env,
    fs,
    io::{self, Read, Write},
    path::{Path, PathBuf},
    time::{Duration, SystemTime, UNIX_EPOCH},
};

pub struct CertD {
    base: PathBuf,
}

impl CertD {
    pub fn new() -> io::Result<CertD> {
        CertD::with_base_dir(
            env::var_os("PGP_CERT_D")
                .map(Into::into)
                .unwrap_or_else(CertD::default_location))
    }

    fn default_location() -> PathBuf {
        // XXX: Other platforms?
        dirs::data_dir()
            .expect("Unsupported platform")
            .join("pgp.cert.d")
    }

    pub fn with_base_dir<P: AsRef<Path>>(base: P) -> io::Result<CertD> {
        Ok(CertD {
            base: base.as_ref().into(),
        })
    }

    fn to_path(&self, fingerprint: String) -> io::Result<PathBuf> {
        // XXX: check fp: all hex, plausible length, make uppercase
        Ok(self.base.join(&fingerprint[..2]).join(&fingerprint[2..]))
    }

    fn to_fingerprint(path: &Path) -> String {
        // XXX: more robust
        let tail = path.file_name().expect("a component");
        let head = path.parent().expect("at least one leg")
            .file_name().expect("a component");
        format!("{}{}", head.to_str().unwrap(), tail.to_str().unwrap())
    }

    pub fn get<F>(&self, fingerprint: F) -> io::Result<Option<(Tag, Data)>>
    where F: ToString,
    {
        if let Ok(mut f) =
            fs::File::open(self.to_path(fingerprint.to_string())?)
        {
            let mut buf = Vec::new();
            f.read_to_end(&mut buf)?;
            Ok(Some((f.metadata()?.modified()?.into(), buf.into())))
        } else {
            Ok(None)
        }
    }

    pub fn get_if_changed<F>(&self, since: Tag, fingerprint: F)
                             -> io::Result<Option<(Tag, Data)>>
    where F: ToString,
    {
        if let Ok(mut f) =
            fs::File::open(self.to_path(fingerprint.to_string())?)
        {
            let modified = f.metadata()?.modified()?.into();
            if modified == since {
                Ok(None) // Not modified.
            } else {
                let mut buf = Vec::new();
                f.read_to_end(&mut buf)?;
                Ok(Some((modified, buf.into())))
            }
        } else {
            Ok(None)
        }
    }

    pub fn insert<F, D>(&self, fingerprint: F, data: D)
                        -> io::Result<()>
    where F: ToString,
          D: AsRef<[u8]>,
    {
        let path = self.to_path(fingerprint.to_string())?;
        idempotent_mkdir(&self.base)?;
        idempotent_mkdir(path.parent().expect("at least one leg"))?;
        fs::File::create(path)?
            .write_all(data.as_ref())
    }

    pub fn iter_fingerprints(&self)
                             -> io::Result<impl Iterator<Item = String>> {
        Ok(fs::read_dir(&self.base)?
           .filter_map(|toplevel| toplevel.ok())
           .filter(|toplevel|
                   toplevel.file_type().map(|t| t.is_dir()).unwrap_or(false)
                   && toplevel.file_name().len() == 2)
           .flat_map(|toplevel| fs::read_dir(toplevel.path()))
           .flat_map(|subdir| subdir.into_iter())
           .filter_map(|entry| entry.ok())
           .map(|entry| Self::to_fingerprint(&entry.path())))
    }

    //pub fn iter<'i>(&'i self)
    //            -> io::Result<impl Iterator<Item = (String, Tag, Data)> + 'i> {
    //    Ok(self.iter_fingerprints()?.filter_map(|fp| {
    //        self.get(&fp).ok().and_then(|v| v).map(|(tag, data)| (fp, tag, data))
    //    }))
    //    //Ok(fs::read_dir(&self.base)?
    //    //   .filter_map(|toplevel| toplevel.ok())
    //    //   .filter(|toplevel|
    //    //           toplevel.file_type().map(|t| t.is_dir()).unwrap_or(false)
    //    //           && toplevel.file_name().len() == 2)
    //    //   .flat_map(|toplevel|
    //    //             fs::read_dir(toplevel.path()))
    //    //   .map(|entry| (Self::to_fingerprint(entry.path()),
    //    //                 unimplemented!(),
    //    //                 Vec::new().into())))
    //    //for toplevel in fs::read_dir(&self.base)? {
    //    //    let toplevel = toplevel?;
    //    //    if ! (toplevel.file_type()?.is_dir()
    //    //          && toplevel.file_name().len() == 2) {
    //    //        continue;
    //    //    }
    //    //}
    //}
}

fn idempotent_mkdir<P: AsRef<Path>>(path: P) -> io::Result<()> {
    if let Err(e) = fs::create_dir(path.as_ref()) {
        if e.kind() == io::ErrorKind::AlreadyExists {
            // Fine.
            Ok(())
        } else {
            // Not fine.
            Err(e)
        }
    } else {
        Ok(())
    }
}

pub type Data = Box<[u8]>;

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Tag(u64);

impl From<SystemTime> for Tag {
    fn from(t: SystemTime) -> Self {
        let d = t.duration_since(UNIX_EPOCH).unwrap_or(Duration::new(0, 0));
        Tag(d.as_secs() ^ d.subsec_nanos() as u64) // XXX better hashing
    }
}

impl From<u64> for Tag {
    fn from(t: u64) -> Self {
        Tag(t)
    }
}

impl From<Tag> for u64 {
    fn from(t: Tag) -> Self {
        t.0
    }
}

#[cfg(test)]
mod tests {
}
